<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap"><i class="mdi mdi-navigation"></i> <span class="hide-menu">Navigation</span></li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}dashboard"
                        aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}dispan/data-produksi"
                        aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data
                            Produksi</span></a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ base_url('') }}dispan/data-laporan"
                        aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data
                            Laporan</span></a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="data-komoditi" aria-expanded="false"><i
                            class="mdi mdi-border-none"></i><span class="hide-menu">Data Komoditi</span></a>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
