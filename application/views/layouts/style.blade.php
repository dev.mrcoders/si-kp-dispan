
<link rel="icon" type="image/png" sizes="16x16" href='@asset("assets/images/favicon.png")'>
<link href="@asset('assets/')dist/css/style.min.css" rel="stylesheet">
@if($classes != 'login')
<!-- Custom CSS -->
<link href="@asset('assets/')assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
<link href="@asset('assets/')assets/extra-libs/c3/c3.min.css" rel="stylesheet">
<link href="@asset('assets/')assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
<!-- Custom CSS -->
{{-- <link href="@asset('assets/')form/isia-form-repeater.min.css" rel="stylesheet"> --}}

<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.4.0/css/responsive.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.3.1/css/fixedHeader.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.3.0/css/rowGroup.dataTables.min.css">
<link href="@asset('assets/')assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css" integrity="sha512-uLI05NEY4Yj4tbrsvcBHTcRJBT4gZaxENUHwjWMcLIK0xaVzpr4ScBA5Wc7dgw/wVTzKLGWsq0MeXQp0SkXpIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="@asset('assets/')assets/libs/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">

<style>
	.auth-wrapper .auth-box {
		width: 100%;
	}
	.w-15 {
		width: 15% !important;
	}

	.w-10 {
		width: 10% !important;
	}
</style>
@endif
<script src="@asset('assets/')assets/libs/jquery/dist/jquery.min.js"></script>
