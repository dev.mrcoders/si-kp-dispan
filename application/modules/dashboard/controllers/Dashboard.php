<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
	var $data = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
	}
	public function index()
	{
		$this->data['title'] = 'Dashboard';
		$this->data['breadcrumb']='dashboard';
		$this->data['welcome']=[
			'dispan'=>"Dinas Pertanian Tanaman Pangan & Hortikultura",
			'disnak'=>"Dinas Peternakan",
			'diskan'=>"Dinas Perikanan",
			'disbun'=>"Dinas Perkebunan"
		];
		$this->view('dashboardv', $this->data);
	}
}

/* End of file Login.php */
/* Location: ./application/modules/Login/controllers/Login.php */
