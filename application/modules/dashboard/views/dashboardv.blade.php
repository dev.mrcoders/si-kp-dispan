@extends('layouts.index')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="@asset('assets/')assets/images/users/5.jpg" width="150" class="rounded-circle" alt="user" />
                        <h4 class="m-t-20 m-b-0">{{ userdata()->nama }}</h4>
                        <a href="mailto:danielkristeen@gmail.com"></a>
                    </div>
                    <div class="badge badge-pill badge-light font-16">Role : {{ userdata()->nama_role }}</div>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="card bg-dark">
                <img src="@asset('assets/')bg-1.jpeg" alt="" class="img-responsive img-fluid w-100">
                <div class="card-img-overlay">
                    <h2 class="font-weight-bold text-uppercase text-dark text-center">Selamat datang di website {{ $welcome[is_subdomain()] }} Kabupaten Kampar</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(function () {
		$.session.remove('tabs');
	});
</script>
@endsection
