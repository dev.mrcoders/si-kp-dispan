@extends('layouts.index')
@section('content')
    <style>
        .my-select2 {
            width: 100%;
            height: 35px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tahun</label>
                                        <select name="" id="ddlYears" class="form-control ">
                                            <option value="">==Pilih Tahun==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Bulan</label>
                                        <select name="" id="ddlMonth" class="form-control">
                                            <option value="">==Pilih Bulan==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Kecamatan</label>
                                        <select name="" id="kecamatan" class="form-control">
                                            <option value="">==Pilih Kecamatan==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Komoditi</label>
                                        <select name="" id="komoditi" class="form-control">
                                            <option value="">==Pilih Komoditi==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group pt-1">
                                        <button type="button" class="btn btn-primary filterData mt-4"> <i
                                                class="mdi mdi-filter"></i> Filter Data</button>
                                        <button type="button" class="btn btn-dark mt-4 resetFilter"><i
                                                class="mdi mdi-filter-remove"></i> Kosongkan Filter</button>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row mb-4">
                            <div class="col-8">
                                <h4 class="card-title">Data produksi Padi</h4>
                            </div>
                            <div class="col-4 text-right">
                                <button class="btn btn-sm btn-info new-data" data-toggle="modal"
                                    data-target=".bs-example-modal-lg"><i class="mdi mdi-plus-box-outline"></i>
                                    Tambah</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table nowrap" id="tb-data" width="100%">
                                <thead class="bg-info text-white">
                                    <tr>
                                        <th id="no">No</th>
                                        <th id="tgl">Tanggal</th>
                                        <th>Kecamatan</th>
                                        <th>Komoditi</th>
                                        <th>Tanam (Ha)</th>
                                        <th>Panen (Ha)</th>
                                        <th>PRO</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="tb-body">

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="v-form-modal" tabindex="" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" data-keyboard="false"
        data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myLargeModalLabel">Masukan Data Produksi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ forms('#', ['id' => 'formProduksi']) }}
                <input type="hidden" name="id_" value="">
                <input type="hidden" name="mode" value="">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="">Tahun</label>
                            {{ _input(['type' => 'select', 'name' => 'tahun', 'class' => 'tahun', 'id' => 'tahun', 'label' => 'Tahun']) }}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Bulan</label>
                            {{ _input(['type' => 'select', 'name' => 'bulan', 'class' => 'bulan', 'id' => 'bulan', 'label' => 'Bulan']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">Kecamatan</label>
                            {{ _input(['type' => 'select', 'name' => 'kd_kecamatan', 'class' => 'kd_kecamatan', 'id' => 'kd_kecamatan', 'label' => 'Kecamatan']) }}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">Komoditi</label>
                            {{ _input(['type' => 'select', 'name' => 'id_komoditi', 'class' => 'id_komoditi', 'id' => 'id_komoditi', 'label' => 'Komoditi']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="">T (Luas Tanam Ha)</label>
                            {{ _input(['type' => 'text', 'name' => 't_', 'class' => 't_ number', 'id' => 't_']) }}
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="">P (Luas Panen Ha)</label>
                            {{ _input(['type' => 'text', 'name' => 'p_', 'class' => 'p_ number', 'id' => 'p_']) }}
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="">Produksi</label>
                            {{ _input(['type' => 'text', 'name' => 'pro_', 'class' => 'pro_ number', 'id' => 'pro_']) }}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left"
                        data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-info waves-effect text-left btn-simpan">simpan</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <script>
        const monthNames = ["Januari", "Febuari", "Maret", "April", "Mei", "Juni", "Juli", "Agusturs", "September",
            "Oktober", "November", "Desember"
        ];
        const years = [];
        const months = [];
        const currentYear = (new Date()).getFullYear();
        const currentMonth = new Date().getMonth();

        $(document).ready(function() {
            ajaxcsrf();
            LoadTables();

            function LoadTables() {
                $('#tb-data').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "dispan/data-produksi/tables",
                        "type": "POST",
                        "data": {
                            csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
							data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                            data.tahun = $('#ddlYears').val();
                            data.bulan = $('#ddlMonth').val();
                            data.kecamatan = $('#kecamatan').val();
                            data.komoditi = $('#komoditi').val();
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    }
                });
            }

            $('.filterData').on('click', function() {
                LoadTables();
            });

            $('.resetFilter').on('click', function() {
                $("#ddlYears, #ddlMonth, #kecamatan, #komoditi").val(null).trigger("change");
                LoadTables();
            });

            $('input.number').keyup(function(event) {
                if (event.which >= 37 && event.which <= 40) return;
                $(this).val(function(index, value) {
                    return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            });

            $('.new-data').on('click', function() {
                $('.modal-title').text('Masukan Data Produksi');
                $('[name="mode"]').val('add');
                $('[name="id_"]').val(0);
                $("#tahun, #bulan, #kd_kecamatan, #id_komoditi").val(null).trigger("change");
                $("#t_, #p_, #pro_").val('');
                $('#v-form-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            })

            $('tbody').on('click', '.edit', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                $('.modal-title').text('Ubah Data Produksi');
                $.ajax({
                    url: base_url + 'dispan/data-produksi/databyid',
                    type: 'get',
                    data: {
                        id: id,
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
                    },
                    success: function(response) {
                        $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                        $('[name="mode"]').val('edit');
                        $('[name="id_"]').val(id);

                        let blnthn = response.data.bln_thn.split("-");
                        var Thn = new Option(blnthn[0], blnthn[0], true, true);
                        var Bln = new Option(monthNames[parseInt(blnthn[1], 10) - 1], blnthn[1],
                            true, true);
                        var OpsKec = new Option(response.data.nama_kecamatan, response.data
                            .kd_kecamatan, true, true);
                        var OpsKom = new Option(response.data.nama_komoditi, response.data
                            .id_komoditi, true, true);
                        $('[name="bulan"]').append(Bln).trigger('change');
                        $('[name="tahun"]').append(Thn).trigger('change');
                        $('[name="kd_kecamatan"]').append(OpsKec).trigger('change');
                        $('[name="id_komoditi"]').append(OpsKom).trigger('change');
                        $('[name="t_"]').val(response.data.p_);
                        $('[name="p_"]').val(response.data.t_);
                        $('[name="pro_"]').val(response.data.pro_);

                    }
                });
                $('#v-form-modal').modal('show');
            });

            $('tbody').on('click', '.delete', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Apa Anda Yakin?',
                    text: "Anda Tidak dapat Mengembalikannya Kembali!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Iya, Hapus itu!',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: base_url + 'dispan/data-produksi/delete',
                            type: 'GET',
                            data: {
                                id: id,
								csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                            },
                            success: function(response) {
								$('meta[name="csrf-token"]').attr("content", response.csrf_param);
                                LoadTables();
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            },
                            error: function(xhr, status, error) {
                                toastr.error(status + " " + xhr.status + " " + error);
                                console.log(xhr.responseText);
                            }
                        });
                    }
                })
            });

            $("#formProduksi").validate({
                rules: {
                    tahun: "required",
                    bulan: "required",
                    kd_kecamatan: "required",
                    id_komoditi: "required",
                    t_: "required",
                    p_: "required",
                    pro_: "required",
                },
                messages: {
                    tahun: "Wajib Di Pilih",
                    bulan: "Wajib Di Pilih",
                    kd_kecamatan: "Wajib Di Pilih",
                    id_komoditi: "Wajib Di Pilih",
                    t_: "Wajib Di Isi",
                    p_: "Wajib Di Isi",
                    pro_: "Wajib Di Isi",
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: submitProduksi,
            });

            function submitProduksi() {
                const button = $('.btn-simpan');

                $.ajax({
                    type: "POST",
                    url: base_url + "dispan/data-produksi/save",
                    data: $('#formProduksi').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content"),
                    dataType: "JSON",
                    beforeSend: function() {
                        button.text("Menyimpan..."); //change button text
                        button.attr("disabled", true); //set button disable
                    },
                    success: function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
                        if (response.success) {
                            $('#v-form-modal').modal('hide');
                            LoadTables();
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.message,
                            })

                        }
                    },
                    complete: function() {
                        button.text("Simpan"); //change button text
                        button.attr("disabled", false); //set button enable
                    },
                    error: function(xhr, status, error) {
                        toastr.error(status + " " + xhr.status + " " + error);
                        console.log(xhr.responseText);
                    }
                });
            }

            for (var i = currentYear; i >= 2015; i--) {
                years.push({
                    'id': i,
                    'text': i
                });
            }

            for (var month = 0; month <= 11; month++) {
                bln = ("0" + (month + 1)).slice(-2);
                months.push({
                    'id': bln,
                    'text': monthNames[month]
                });
            }

            $('#ddlYears, #tahun').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih tahun==',
                data: years
            });

            $('#ddlMonth, #bulan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih Bulan==',
                data: months
            });

            $('#kecamatan, #kd_kecamatan').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama kecamatan',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'select2-kecamatan',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });

            $('#komoditi, #id_komoditi').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama komoditi',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'dispan/select2-komoditi',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });


        });
    </script>
@endsection
