@extends('layouts.index')
@section('content')
    <style>
        table thead tr th {
            font-size: 10px;
            text-align: center !important;

        }

        table tbody td {
            font-size: 10px;
            text-align: center !important;
        }

        table tfoot th {
            font-size: 10px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tahun</label>
                                        <select name="" id="ddlYears" class="form-control ">
                                            <option value="">==Pilih Tahun==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Komoditi</label>
                                        <select name="" id="komoditi" class="form-control">
                                            <option value="">==Pilih Komoditi==</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <button type="button" id="filter" class="btn btn-primary mt-4"><i
                                                class="fa fa-filter"></i> Filter Data</button>
                                        <button type="button" id="hapusfilter" class="btn btn-dark mt-4">Kosongkan
                                            Filter</button>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <button type="button" id="hapusfilter" class="btn waves-effect waves-light btn-outline-danger ">Kosongkan Filter</button>
                                                    </div>
                                                </div> -->
                            </div>
                            <!-- <button type="button" class="btn btn-primary cari btn-block">Lihat Data</button> -->
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">Data produksi <span id="title"></span></h4>
                            </div>



                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered nowrap" id="tb-data" width="100%">
                                <thead class="bg-info text-white text-center ">
                                    <tr>
                                        <th rowspan="3" style="vertical-align: middle;">No</th>
                                        <th rowspan="3" style="vertical-align: middle;">Kecamatan</th>
                                        <th colspan="18">Bulan</th>
                                        <th rowspan="2" colspan="3" style="vertical-align: middle;">Jumlah <br> (Jan
                                            s/d Jun)</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">JAN</th>
                                        <th colspan="3">FEB</th>
                                        <th colspan="3">MAR</th>
                                        <th colspan="3">APR</th>
                                        <th colspan="3">MEI</th>
                                        <th colspan="3">JUN</th>

                                    </tr>
                                    <tr>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                    </tr>
                                </thead>
                                <tbody id="tb-body" class="border border-info">


                                </tbody>
                                <tfoot align="right">
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <?php for ($i = 0; $i < 7; $i++) { ?>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>

                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-8">
                                <h4 class="card-title">Data produksi <span id="title2"></span></h4>
                            </div>



                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered nowrap" id="tb-data2" width="100%">
                                <thead class="bg-info text-white text-center ">
                                    <tr>
                                        <th rowspan="3" style="vertical-align: middle;">No</th>
                                        <th rowspan="3" style="vertical-align: middle;">Kecamatan</th>
                                        <th colspan="18">Bulan</th>
                                        <th rowspan="2" colspan="3" style="vertical-align: middle;">Jumlah <br> (Jul
                                            s/d Des)</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">JUL</th>
                                        <th colspan="3">AGU</th>
                                        <th colspan="3">SEP</th>
                                        <th colspan="3">OKT</th>
                                        <th colspan="3">NOV</th>
                                        <th colspan="3">DEC</th>

                                    </tr>
                                    <tr>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                        <th>T</th>
                                        <th>P</th>
                                        <th>PRO</th>
                                    </tr>
                                </thead>
                                <tbody id="tb-body" class="border border-info">


                                </tbody>
                                <tfoot align="right">
                                    <tr>
                                        <th colspan="2">Total</th>
                                        <?php for ($i = 0; $i < 7; $i++) { ?>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>

                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        const years = [];
        const currentYear = (new Date()).getFullYear();
        $(function() {
           
            var Thn = new Option(currentYear, currentYear, true, true);
            var OpsKom = new Option('Padi', '3', true, true);
            $('#ddlYears').append(Thn).trigger('change');
            $('#komoditi').append(OpsKom).trigger('change');
            $('#title').text('Padi');
            $('#title2').text('Padi');
            for (var i = currentYear; i >= 2015; i--) {
                years.push({
                    'id': i,
                    'text': i
                });
            }

            $('#ddlYears').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: '==Pilih tahun==',
                data: years
            });

            $('#komoditi').select2({
                width: '100%',
                theme: 'bootstrap4',
                allowClear: true,
                placeholder: 'masukkan nama komoditi',
                ajax: {
                    dataType: 'json',
                    url: base_url + 'dispan/select2-komoditi',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        }
                    },
                    processResults: function(data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });

            DataTables();

            $('#filter').on('click', function() {
                // $('#title').text('');
                if ($('#ddlYears').val() == '') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Pilih Tahun Dahulu!',
                    });
                } else if ($('#komoditi').val() == '') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Pilih Komoditi Dahulu!',
                    });
                } else {
                    DataTables();
                    let Data = $('#komoditi').select2('data');
                    $('#title').text(Data[0]['text']);
                    $('#title2').text(Data[0]['text']);
                }
            });

            var filter = [$("#ddlYears"), $("#komoditi")];
            $("#hapusfilter").on('click', function() {
                $.each(filter, function(index, value) {
                    value.val(null).trigger('change');
                });
                $('#title2, #title').text('');
                DataTables();

            });

            function DataTables() {
                $('#tb-data').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    paging: false,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "dispan/data-laporan/tables/1",
                        "type": "POST",
                        "data": {
                            csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                            data.tahun = $('#ddlYears').val();
                            data.komoditi = $('#komoditi').val();
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    },
                    "footerCallback": function(row, data, start, end, display) {
                        var api = this.api(),
                            data;

                        // converting to interger to find total
                        var intVal = function(i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };

                        let Totals = [];
                        let cols = [];
                        for (let i = 2; i <= 22; i++) {
                            cols[i] = i;
                            Totals[i] = api
                                .column(i)
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);
                        }

                        // Update footer by showing the total with the reference of the column index 
                        let arrT = [];
                        let arrP = [];
                        let arrPRO = [];
                        let t = 0;
                        let p = 0;
                        let pro = 0;
                        for (let c = 2; c <= 22; c++) {
                            $(api.column(parseInt(c)).footer()).html(Totals[c]);
                            // $('tr:eq(1) td:eq(2)', api.table().footer()).html('a');
                            // 

                            if (c == 2 || c == 5 || c == 8 || c == 11 || c == 14 || c == 17) {
                                t += Totals[c];
                                arrT.push(t);
                            }
                            if (c == 3 || c == 6 || c == 9 || c == 12 || c == 15 || c == 18) {
                                p += Totals[c];
                                arrP.push(p);
                            }
                            if (c == 4 || c == 7 || c == 10 || c == 13 || c == 16 || c == 19) {
                                pro += Totals[c];
                                arrPRO.push(pro);
                            }

                        }


                    },

                });
                $('#tb-data2').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    paging: false,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "dispan/data-laporan/tables/2",
                        "type": "POST",
                        "data": {
                            csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                            data.tahun = $('#ddlYears').val();
                            data.komoditi = $('#komoditi').val();
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    },
                    "footerCallback": function(row, data, start, end, display) {
                        var api = this.api(),
                            data;

                        // converting to interger to find total
                        var intVal = function(i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                i : 0;
                        };

                        let Totals = [];
                        let cols = [];
                        for (let i = 2; i <= 22; i++) {
                            cols[i] = i;
                            Totals[i] = api
                                .column(i)
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);
                        }

                        // Update footer by showing the total with the reference of the column index 
                        let arrT = [];
                        let arrP = [];
                        let arrPRO = [];
                        let t = 0;
                        let p = 0;
                        let pro = 0;
                        for (let c = 2; c <= 22; c++) {
                            $(api.column(parseInt(c)).footer()).html(Totals[c]);
                            // $('tr:eq(1) td:eq(2)', api.table().footer()).html('a');
                            // 

                            if (c == 2 || c == 5 || c == 8 || c == 11 || c == 14 || c == 17) {
                                t += Totals[c];
                                arrT.push(t);
                            }
                            if (c == 3 || c == 6 || c == 9 || c == 12 || c == 15 || c == 18) {
                                p += Totals[c];
                                arrP.push(p);
                            }
                            if (c == 4 || c == 7 || c == 10 || c == 13 || c == 16 || c == 19) {
                                pro += Totals[c];
                                arrPRO.push(pro);
                            }

                        }
                    },

                });
            }


        });
    </script>
@endsection
