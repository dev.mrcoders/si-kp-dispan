@extends('layouts.index')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">

                    <div class="card-body ">
                        <div class="row mb-4">
                            <div class="col-8">
                                <h4 class="card-title">Data Komoditi</h4>
                            </div>
                            <div class="col-4 text-right">
                                <button class="btn btn-sm btn-info new-data" data-toggle="modal"
                                    data-target=".bs-example-modal-lg"><i class="mdi mdi-plus-box-outline"></i>
                                    Tambah</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table nowrap" id="tb-data" width="100%">
                                <thead class="bg-info text-white">
                                    <tr>
                                        <th class="col-1">No</th>
                                        <th>Nama Komoditi</th>
                                        <th class="col-1">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Data Komoditi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {{ forms('#', ['id' => 'form-komoditi']) }}
                <input type="hidden" name="id_" value="">
                <div class="modal-body" id="fields-komoditi">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" id="komoditi" name="komoditi[]"
                                    placeholder="Nama Komoditi" required>
                            </div>
                        </div>
                        <div class="col-sm-2" id="btn-row-add">
                            <div class="form-group">
                                <button class="btn btn-success" type="button" onclick="education_fields();"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(function() {
            ajaxcsrf();

            LoadTables();

            function LoadTables() {
                $('#tb-data').DataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
                    },
                    stateSave: true,
                    destroy: true,
                    "processing": true,
                    "serverSide": true,
                    "order": [],
                    "ajax": {
                        "url": base_url + "dispan/data-komoditi/tables",
                        "type": "POST",
                        "data": {
                            csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                        },
                        "data": function(data) {
                            data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
                        },
                        "dataSrc": function(response) {
                            $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                            return response.data;
                        },
                    }
                });
            }

            $.extend($.validator.prototype, {
                checkForm: function() {
                    this.prepareForm();
                    for (var i = 0, elements = (this.currentElements = this.elements()); elements[
                            i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(
                                elements[i].name).length > 1) {
                            for (var cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                }
            });

            $("#form-komoditi").validate({
                rules: {
                    "komoditi[]": {
                        required: true
                    },

                },
                messages: {
                    "komoditi[]": {
                        required: "Wajib Di isi"
                    },
                },
                errorElement: "em",
                errorPlacement: function(error, element) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("invalid-feedback");
                    if (element.hasClass("select2")) {
                        error.insertAfter(element.next("span"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-invalid").removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid").removeClass("is-invalid");
                },
                submitHandler: submitKomoditi,
            });

            $('.new-data').on('click', function(event) {
                event.preventDefault();
                $('form#form-komoditi')[0].reset();
                $('[name="id_"]').val(0);
                $('#btn-row-add').show('fast');
                $('#add-modal').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            });

            $('tbody').on('click', '.edit', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                $.ajax({
                    url: base_url + 'dispan/data-komoditi/databyid',
                    type: 'get',
                    data: {
                        id: id,
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                    },
                    success: function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
                        $('[name="id_"]').val(id);
                        $('#komoditi').val(response.data.nama_komoditi);
                        $('#btn-row-add').hide('fast');
                        $('#add-modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        })
                    }
                });

            });

            $('tbody').on('click', '.delete', function(event) {
                event.preventDefault();
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Apa Kamu Yakin?',
                    text: "Kamu Tidak Bisa Mengembalikan Datanya Kembali",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Iya, Hapus itu!',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: base_url + 'dispan/data-komoditi/delete',
                            type: 'GET',
                            data: {
                                id: id,
								csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
                            },
                            success: function(response) {
                                $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                                if (response.success) {
                                    LoadTables();
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: response.message,
                                        showConfirmButton: false,
                                        timer: 1500
                                    })
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: response.message,
                                    })
                                }


                            }
                        });

                    }
                })

            });

            function submitKomoditi() {
                const button = $('.btn-simpan');
                let Form = $('#form-komoditi').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
                $.ajax({
                    url: base_url + "dispan/data-komoditi/save",
                    type: 'POST',
                    data: Form,
                    beforeSend: function() {
                        button.text("Menyimpan..."); //change button text
                        button.attr("disabled", true); //set button disable
                    },
                    success: function(response) {
                        $('meta[name="csrf-token"]').attr("content", response.csrf_param);
                        if (response.success) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Data Berhasil Disimpan',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            $('#add-modal').modal('hide');
                            LoadTables();

                        }
                    },
                    complete: function() {
                        button.text("Simpan"); //change button text
                        button.attr("disabled", false); //set button enable
                    },
                    error: function(xhr, status, error) {
                        toastr.error(status + " " + xhr.status + " " + error);
                        console.log(xhr.responseText);
                    }
                });
            }

        });

        var room = 1;

        function education_fields() {

            room++;
            var objTo = document.getElementById('fields-komoditi')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "row removeclass" + room);
            var rdiv = 'removeclass' + room;
            divtest.innerHTML =
                '<div class="col-sm-10"><div class="form-group"><input type="text" class="form-control" id="Schoolname" name="komoditi[]" placeholder="Nama Komoditi" required></div></div><div class="col-sm-2"> <div class="form-group"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' +
                room + ');"> <i class="fa fa-minus"></i> </button> </div></div></div>';

            objTo.appendChild(divtest)
        }

        function remove_education_fields(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>
@endsection
