<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Komoditi extends MY_Controller
{
	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->load->model('M_komoditi');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Master Komoditi';
		$this->view('komoditi.index', $this->data);
	}

	public function DataTablesKomoditi($value = '')
	{
		$list = $this->M_komoditi->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $r->nama_komoditi;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs  edit" data-id="' . $r->id_komoditi . '"><i class="mdi mdi-pencil-box"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 delete" data-id="' . $r->id_komoditi . '"><i class="mdi mdi-delete"></i> Hapus</button>';

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_komoditi->count_all(),
			"recordsFiltered" => $this->M_komoditi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Save()
	{
		foreach ($this->input->post('komoditi') as $key => $value) {
			$FormData[] = [
				'nama_komoditi' => $value
			];
		}
		if ($this->input->post('id_') == 0) {
			$this->M_komoditi->Created($FormData);
			$this->json = [
				'success' => true,
				'message' => "Data Berhasil Di Simpan",
				'data' => []
			];
		} else {
			foreach ($FormData as $key => $v) {
				$FormData[$key]['id_komoditi'] = $this->input->post('id_');
			}
			$this->M_komoditi->Updated($FormData);
			$this->json = [
				'success' => true,
				'message' => "Data Berhasil Di Ubah",
				'data' => []
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataById()
	{
		$Data = $this->M_komoditi->GetDataById();
		$this->json = [
			'success' => true,
			'message' => "Data Berhasil Di Hapus",
			'data' => $Data
		];
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{

		$hapus = $this->M_komoditi->Deleted($this->input->get('id'));

		if ($hapus) {
			$this->json = [
				'success' => true,
				'message' => "Data Berhasil Di Hapus",
				'data' => []
			];
		} else {
			$this->json = [
				'success' => false,
				'message' => "Data Komoditi Tidak Dapat Dihapus Karena Berkaitan Dengan Data Produksi",
				'data' => []
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function OptionSelect2()
	{
		$this->db->like('nama_komoditi', $this->input->get('search'), 'BOTH');
		$Data = $this->db->get('tb_komoditi')->result();
		foreach ($Data as $key => $value) {
			$Res[] = [
				'id' => $value->id_komoditi,
				'text' => $value->nama_komoditi
			];
		}

		$NewRes = [
			'id' => ' ',
			'text' => '==Pilih komoditi=='
		];
		array_unshift($Res, $NewRes);
		$this->output->set_content_type('application/json')->set_output(json_encode($Res));
	}
}

/* End of file Produksi.php */
