<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends MY_Controller
{
	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Master Komoditi';
		$this->view('produksi.laporan', $this->data);
	}

	public function DataTables($bul)
	{
		$t_sum_fo = 0;
		$p_sum_fo = 0;
		$pro_sum_fo = 0;

		$data = array();
		if ($bul == 1) {
			$bulan = ['01', '02', '03', '04', '05', '06'];
		} else {
			$bulan = ['07', '08', '09', '10', '11', '12'];
		}
		$this->db->where('kd_kecamatan !=', 0);
		$kecamatan = $this->db->get_where('tb_kecamatan')->result();
		foreach ($kecamatan as $key => $v) {
			$t_sum = 0;
			$p_sum = 0;
			$pro_sum = 0;
			$row = array();
			$row[] = $key + 1;
			$row[] = $v->nama_kecamatan . $this->input->get('tahun');;
			$t = 0;
			$p = 0;
			$pro = 0;
			for ($i = 0; $i < 6; $i++) {
				if ($this->input->post('tahun')) {
					$this->db->where('SUBSTRING(bln_thn,1,4)', $this->input->post('tahun'));
				} else {
					$this->db->where('SUBSTRING(bln_thn,1,4)', date('Y'));
				}
				if ($this->input->post('komoditi')) {
					$this->db->where('id_komoditi', $this->input->post('komoditi'));
				} else {
					$this->db->where('id_komoditi', 1);
				}
				$prod = $this->db->get_where('v_produksi', ['kd_kecamatan' => $v->kd_kecamatan, 'SUBSTRING(bln_thn,6,2)' => $bulan[$i]]);
				$data2 = array();
				$t = ($prod->row()->t_ == '' ? 0 : $prod->row()->t_);
				$p = ($prod->row()->p_ == '' ? 0 : $prod->row()->p_);
				$pro = ($prod->row()->pro_ == '' ? 0 : $prod->row()->pro_);

				$row[] = $t;
				$row[] = $p;
				$row[] = $pro;

				$t_sum += $t;
				$p_sum += $p;
				$pro_sum += $pro;
			}
			$row[] = $t_sum;
			$row[] = $p_sum;
			$row[] = $pro_sum;
			$data[] = $row;
		}

		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($kecamatan),
			"recordsFiltered" => count($kecamatan),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}
}