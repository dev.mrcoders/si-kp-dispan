<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Produksi extends MY_Controller
{
	var $data = [];
	var $user;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];
		$this->load->model('M_data', 'pro');
		$this->user = userdata()->nip;
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Data Master Produksi';
		$this->view('produksi.index', $this->data);
	}

	public function DataTableProduksi()
	{
		$list = $this->pro->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		//looping data mahasiswa
		foreach ($list as $pro) {
			$no++;
			$row = array();
			//row pertama akan kita gunakan untuk btn edit dan delete
			$row[] = $no;
			$row[] = date('M Y', strtotime($pro->bln_thn));
			$row[] = $pro->nama_kecamatan;
			$row[] = $pro->nama_komoditi;
			$row[] = $pro->t_ . ' Ha';
			$row[] = $pro->p_ . ' Ha';
			$row[] = $pro->pro_;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $pro->id_data_pro . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $pro->id_data_pro . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->pro->count_all(),
			"recordsFiltered" => $this->pro->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Save()
	{
		$PostData = [
			'kd_kecamatan' => $this->input->post('kd_kecamatan'),
			'id_komoditi' => $this->input->post('id_komoditi'),
			'bln_thn' => $this->input->post('tahun') . '-' . $this->input->post('bulan')
		];
		$IsDataExists = $this->pro->IsData($PostData);
		if ($this->input->post('mode') == 'add') {
			if ($IsDataExists) {
				$this->json = [
					'success' => false,
					'message' => "Data Tahun Bulan Kecamatan dan Komoditi yang anda inputkan sudah ada",
					'data' => ['data' => $IsDataExists]
				];
			} else {
				$PostData['t_'] = $this->input->post('t_');
				$PostData['p_'] = $this->input->post('p_');
				$PostData['pro_'] = $this->input->post('pro_');
				$PostData['created_user'] = $this->user;
				$PostData['created_ad'] = date('Y-m-d');

				$this->pro->SavedData($PostData);
				$this->json = [
					'success' => true,
					'message' => "Data Berhasil Di Simpan",
					'data' => []
				];
			}
		} else {
			$PostData['t_'] = $this->input->post('t_');
			$PostData['p_'] = $this->input->post('p_');
			$PostData['pro_'] = $this->input->post('pro_');
			$PostData['created_user'] = $this->user;
			$PostData['created_ad'] = date('Y-m-d');

			$this->pro->UpdatedData($PostData,$this->input->post('id_'));
			$this->json = [
				'success' => true,
				'message' => "Data Berhasil Di Ubah",
				'data' => []
			];
		}
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function DataById()
	{
		$data = $this->pro->IsData(['id_data_pro'=>$this->input->get('id')]);
		$this->json= [
			'success' => true,
			'message' => "Data Ditemukan",
			'data' => $data
		];
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$this->pro->DeletedData($this->input->get('id'));
		$this->json= [
			'success' => true,
			'message' => "Data Berhasil Dihapus",
			'data' => []
		];
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}
}

/* End of file Produksi.php */
