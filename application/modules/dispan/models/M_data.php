<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_data extends CI_Model
{
    //set nama tabel yang akan kita tampilkan datanya
    var $table = 'v_produksi';
    var $table_master = 'tb_data_produksi';
    //set kolom order, kolom pertama saya null untuk kolom edit dan hapus
    var $column_order = array(null, 'kd_kecamatan', 'nama_kecamatan', 'nama_komoditi','bln_thn','id_komoditi');

    var $column_search = array('kd_kecamatan', 'nama_kecamatan', 'nama_komoditi','bln_thn','id_komoditi');
    // default order 
    var $order = array('id_data_pro' => 'desc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        if ($this->input->post('bulan') ) {
            $this->db->where('SUBSTRING(bln_thn,6,2) = "'.$this->input->post('bulan').'"');
        } 
        if ($this->input->post('tahun')) {
            $this->db->where('SUBSTRING(bln_thn,1,4) = "'.$this->input->post('tahun').'"');
        }
        if ($this->input->post('kecamatan')) {
            $this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
        }
        if ($this->input->post('komoditi')) {
            $this->db->where('id_komoditi', $this->input->post('komoditi'));
        }
        $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function IsData($data)
	{
		return $this->db->get_where($this->table,$data)->row();
	}

	public function SavedData($data)
	{
		return $this->db->insert($this->table_master, $data);
	}

	public function UpdatedData($data,$id)
	{
		$this->db->where('id_data_pro', $id);
		return $this->db->update($this->table_master, $data);
	}

	public function DeletedData($id)
	{
		$this->db->where('id_data_pro', $id);
		return $this->db->delete($this->table_master);
		
	}
}
