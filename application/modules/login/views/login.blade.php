@extends('layouts.index')
@section('login')
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center"
        style="background:url('@asset('assets/')bg-1.jpeg') no-repeat center center;background-size: cover;">
        <div class="auth-box on-sidebar" style="position: relative;">
            <div id="loginform">
                <div class="logo">
                    <span class="db"><img src="@asset('assets/')logo_kampar.png" alt="logo" class="w-50" /></span>
                    <h5 class="font-medium m-b-20">Sign In to Admin</h5>
                </div>

                <div class="row">
                    <div class="col-12">
                        {{ forms('', ['id' => 'loginform', 'class' => 'form-horizontal m-t-20']) }}

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                            </div>
                            <input type="text" class="form-control form-control-lg username" placeholder="NIP OR EMAIL"
                                name="username" aria-label="username" aria-describedby="basic-addon1">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                            </div>
                            <input type="password" class="form-control form-control-lg password" placeholder="Password"
                                name="password" aria-label="Password" aria-describedby="basic-addon1">
                        </div>

                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-login" type="submit">Log In</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            ajaxcsrf();
            $('form#loginform').on('submit', function(e) {

                e.preventDefault();
                e.stopImmediatePropagation();
                let button = $('.btn-login');
                $.ajax({
                    type: "POST",
                    url: "{{ base_url('login-checking') }}",
                    data: $(this).serialize(),
                    dataType: "JSON",
                    beforeSend: function() {
                        button.html('Prosessing...');
                        button.attr('disabled', true);
                    },
                    success: function(response) {
                        if (response.success) {
                            window.location.href = "{{ base_url() }}" + response.data
                                .redirect
                        } else {
                            $('.username, .password').val();
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: response.message,
                                
                            })
                        }
                        update_csrf_fields(response.data.csrf_token);
                    },
                    complete: function() {
                        setTimeout(() => {
                            button.html('Log In');
                            button.attr('disabled', false);
                        }, 500);

                    }
                });
            });
        });
    </script>
@endsection
