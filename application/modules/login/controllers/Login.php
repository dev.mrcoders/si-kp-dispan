<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{
	var $data = [];
	public function __construct()
	{
		parent::__construct();
		// $this->dbsetup->configure();
		$this->data = ['classes' => $this->router->fetch_class(), 'method' => $this->router->fetch_method()];

		$this->load->model('M_auth', 'AUTH');
	}

	public function index()
	{
		$this->data['title'] = 'LOGIN FORM';
		$this->view('login', $this->data);
	}

	public function CheckLogin()
	{
		$PostForm = [
			'nip' => $this->input->post('username'),
			'password' => md5($this->input->post('password'))
		];

		$IsLogin = $this->AUTH->IsLogin($PostForm);

		if ($IsLogin) {
			$session = array('login' => TRUE, 'data' => $IsLogin);
			$this->session->set_userdata($session);
			$Response = [
				'success' => true,
				'message' => "Login Berhasil",
				'data' => [
					'redirect' => 'dashboard',
					'csrf_token' => $this->security->get_csrf_hash()
				]
			];
		} else {
			$Response = [
				'success' => false,
				'message' => "Nip Atau Password Salah",
				'data' => [
					'csrf_token' => $this->security->get_csrf_hash()
				]
			];
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}

	public function IsLogOut()
	{
		$data_session = array('login' => "", 'data' => '');
		$this->session->unset_userdata($data_session); //clear session
		$this->session->sess_destroy(); //tutup session
		redirect(base_url());
	}
}

/* End of file Controllername.php */
