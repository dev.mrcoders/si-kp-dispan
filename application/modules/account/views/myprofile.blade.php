@extends('layouts.index')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="m-t-30"> <img src="@asset('assets/logo_kampar.png')" class="rounded-circle" width="150" />
                            <h4 class="card-title m-t-10">{{ userdata()->nama }}</h4>
                            <h6 class="card-subtitle">{{ userdata()->nama_role }}</h6>
                        </center>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <div class="card-body">
                        <small class="text-muted">Email address </small>
                        <h6>{{ userdata()->email }}</h6>
                        <small class="text-muted p-t-30 db">Phone</small>
                        <h6>{{ userdata()->phone }}</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <!-- Tabs -->
                    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month"
                                role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                                role="tab" aria-controls="pills-setting" aria-selected="false">Setting</a>
                        </li>
                    </ul>
                    <!-- Tabs -->
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="last-month" role="tabpanel"
                            aria-labelledby="pills-profile-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                        <br>
                                        <p class="text-muted">{{ userdata()->nama }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                        <br>
                                        <p class="text-muted">{{ userdata()->phone }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                        <br>
                                        <p class="text-muted">{{ userdata()->email }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6"> <strong>Jabatan</strong>
                                        <br>
                                        <p class="text-muted">{{ userdata()->jabatan }}</p>
                                    </div>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <form class="form-horizontal form-material" id="form-profil">
                                        <div class="form-group">
                                            <label class="col-md-12">NIP (Username)</label>
                                            <div class="col-md-12">
                                                <input type="text" value="{{ userdata()->nip }}" name="nip"
                                                    class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Full Name</label>
                                            <div class="col-md-12">
                                                <input type="text" value="{{ userdata()->nama }}" name="nama"
                                                    class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Email</label>
                                            <div class="col-md-12">
                                                <input type="email" value="{{ userdata()->email }}"
                                                    class="form-control form-control-line" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Phone No</label>
                                            <div class="col-md-12">
                                                <input type="text" value="{{ userdata()->phone }}" name="phone"
                                                    class="form-control form-control-line">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button class="btn btn-success">Update Profile</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                            <div class="card-body">
                                <form class="form-horizontal form-material" id="form-password">
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">NIP (Username)</label>
                                        <div class="col-md-12">
                                            <input type="text" readonly value="{{ userdata()->nip }}"
                                                class="form-control form-control-line" name="nip">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="input-group col-md-12">
                                            <input type="password" value="" class="form-control form-control-line"
                                                name="password" id="password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="ti-lock"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Retype Password</label>
                                        <div class="input-group col-md-12">
                                            <input type="password" value="" class="form-control form-control-line"
                                                name="re_password">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="ti-lock"></i></span>
                                            </div>
                                        </div>
                                        <small class="feedback col-md-12"></small>
                                    </div>

                                    <div class="custom-control custom-checkbox ml-md-2 mr-md-4 m-b-15 col-sm-12">
                                        <input type="checkbox" class="custom-control-input" id="showPass">
                                        <label class="custom-control-label" for="showPass">Show Password</label>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success update-btn" disabled>Update
                                                Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
    </div>
    <script>
        $(function() {
            ajaxcsrf();
            $('form#form-profil').submit(function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                let FormDatas = $(this).serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]')
                    .attr("content");

                $.ajax({
                    type: "POST",
                    url: base_url + "profile-update",
                    data: FormDatas,
                    dataType: "JSON",
                    success: function(response) {
                        if (response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Update Profile',
                                text: response.message,
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = '{{ base_url() }}' +
                                        response.redirect;
                                }
                            })
                        }
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            });

			$('form#form-password').submit(function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                let FormDatas = $(this).serialize() + "&csrf_hash_name=" + $('meta[name="csrf-token"]')
                    .attr("content");

                $.ajax({
                    type: "POST",
                    url: base_url + "profile-update-password",
                    data: FormDatas,
                    dataType: "JSON",
                    success: function(response) {
                        if (response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Update Password',
                                text: response.message,
                                allowOutsideClick: false
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = '{{ base_url() }}' +
                                        response.redirect;
                                }
                            })
                        }
                    },
                    error: function(request, textStatus, errorThrown) {
                        console.log(request.responseText);
                    }
                });
            });

            $('[name="re_password"]').on('keyup', function() {
                let password = $('[name="password"]').val();
                let confirmPassword = $('[name="re_password"]').val();
                if (password != confirmPassword)
                    $('.feedback').html("Password does not match !").css("color", "red");
                else
                    $('.update-btn').attr('disabled', false);
                	$('.feedback').html("Password match !").css("color", "green");
            });

            $('#showPass').on('click', function() {
                var pass = $('[name="password"]');
                var re_pass = $('[name="re_password"]');
                if (pass.attr('type') === 'password' || re_pass.attr('type') === 'password') {
                    pass.attr('type', 'text');
                    re_pass.attr('type', 'text');
                } else {
                    pass.attr('type', 'password');
                    re_pass.attr('type', 'password');
                }
            })

        });
    </script>
@endsection
