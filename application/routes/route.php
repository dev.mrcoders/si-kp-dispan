<?php
Route::filter('logged_in', function () {

	if ($this->session->userdata('login') == FALSE) {
		$this->session->set_flashdata('session', 'Session Sudah Habis Silahkan Login');
		redirect(base_url());
	}
});

Route::get('login', 'login');
Route::post('login-checking', 'login/CheckLogin');
Route::get('islogout', 			'login/islogout');
Route::get('dashboard', 'dashboard', array('before' => 'logged_in'));
Route::get('select2-kecamatan', 'regions/kecamatan/OptionSelect2', array('before' => 'logged_in'));

Route::get('profile', 'account', array('before' => 'logged_in'));
Route::post('profile-update', 'account/UpdateProfile', array('before' => 'logged_in'));
Route::post('profile-update-password', 'account/UpdatePassword', array('before' => 'logged_in'));

Route::prefix('dispan', function () {
	Route::get('data-produksi', 'dispan/produksi', array('before' => 'logged_in'));
	Route::get('data-laporan', 'dispan/laporan', array('before' => 'logged_in'));
	Route::get('data-komoditi', 'dispan/komoditi', array('before' => 'logged_in'));
	Route::get('select2-komoditi', 'dispan/komoditi/OptionSelect2', array('before' => 'logged_in'));
	Route::prefix('data-produksi', function () {
		Route::post('tables', 'dispan/produksi/DataTableProduksi', array('before' => 'logged_in'));
		Route::post('save', 'dispan/produksi/save', array('before' => 'logged_in'));
		Route::get('databyid', 'dispan/produksi/DataById', array('before' => 'logged_in'));
		Route::get('delete', 'dispan/produksi/Delete', array('before' => 'logged_in'));
	});

	Route::prefix('data-laporan', function () {
		Route::post('tables/(:num)', 'dispan/Laporan/DataTables/$1', array('before' => 'logged_in'));
	});

	Route::prefix('data-komoditi', function () {
		Route::post('tables', 'dispan/komoditi/DataTablesKomoditi', array('before' => 'logged_in'));
		Route::post('save', 'dispan/komoditi/save', array('before' => 'logged_in'));
		Route::get('databyid', 'dispan/komoditi/DataById', array('before' => 'logged_in'));
		Route::get('delete', 'dispan/komoditi/Delete', array('before' => 'logged_in'));
	});
});


